# Wilson Lo Frontend Engineer Technical Assessment
![cover](https://i.imgur.com/Wkui0rs.png)

## Contents
The context of the project
| Name                           | Description                                   | Link             |
| ------------------------------ | --------------------------------------------- | -----------------|
| File Tree Structure            | The file tree structure of the project        | [Link](#file-tree-structure) |
| Functional Limitation (API Bugs Affect) | A backend integrate with firebase functions   | [Link](#functional-limitation-api-bugs-affect) |
| Demo Detail                    | The Demo detail of the assessment             | [Link](#Demo_Detail ) |
| Choice of Package              | Explain the choice of package of the project  | [Link](#choice-of-packagee) |
| Potential Improvement          | Explain the potential improvement of the project | [Link](#potential-improvement) |
| Production Consideration       | Explain the production consideration of the project | [Link](#production-consideration) |
| Assumptions                    | Explain the self assumptions making of the project | [Link](#assumptions) |

## File Tree Structure 
```
.
├── locales
│   ├── en
│   │    └── common.json
│   ├── zh-HK
│   │    └── common.json
│   └── zh-TW
│   │    └── common.json
├── public
├── src
│   ├── apis
│   │    ├── api.ts
│   │    └── https.ts
│   ├── components
│   │    ├── doctorListPageCompontent
│   │    │    ├── banner
│   │    │    ├── leftContainer
│   │    │    └── rightContainer
│   │    ├── globalComponent
│   │    │    ├── header
│   │    │    └── layout
│   │    └── homePageCompontent
│   ├── pages
│   │    ├── doctorList
│   │    ├── _app.tsx
│   │    └── index.tsx
│   └── store
│   │    ├── actions
│   │    │    ├── doctorListActions
│   │    │    ├── globalActions/loadingActions
│   │    │    └── homePageActions
│   │    ├── reducers
│   │    │    ├── doctorListReducer
│   │    │    ├── globalReducer/loadingReducer
│   │    │    └── homePageReducer
│   │    ├── types
│   │    │    ├── doctorListType
│   │    │    ├── globalType/loadingType
│   │    │    └── homePageType
│   │    └── index.ts
├── styles
│   ├── Home.default.css
│   ├── base.css
│   ├── components.css
│   ├── global.css
│   └── utilities.css
├── .gitignore
├── README.md
├── i18n.json
├── next.env.d.ts
├── next.config.js
├── package.json
├── postcss.config.js
├── tailwind.config.js
├── tsconfig.json
├── yarn.loack
```

## Functional Limitation (API Bugs Affect)
| Affect Function in requirement | Description | Affect Function Screen Shot Link | Bug Report Link |
| ------------------------------ | --------------------------------- | ------ | ------ |
| System can't filter booking by date | GET/booking get error (can't get "date" column) | | shorturl.at/qPWY2 shorturl.at/rszEI |
| User can book the appointment by date and time | POST/booking get error (can't save "date" column) | | shorturl.at/hjyOP |
| User can update the appointment by date and time | PATCH/booking get error (can't update any data) | | shorturl.at/gFRWX |

## Demo Detail
URL: https://wilson-lo-frontend-engineer-technical-assessment.vercel.app
[Demo Video](https://www.youtube.com/watch?v=_QdgfJ5x8UI)
[Please take a look the short present video too (Around 15min)](https://www.youtube.com/watch?v=whgZv7-q3yY)

## Choice of Package
1. Typescript - as strong Javascript, Typescript is the best friend of all Frontend Engineer because of its crazy stability (such as die for no reason) with nice maintainability during a large size product (everybody got no freestyle anymore - except you guys use it as Anyscript :D )
2. NextJS  - A great opensource opinionated framework, with a low learning curve, nice document, and development experience, SEO friendly (SSR + SSG), and excellent performance for large size products (Small Size project please use Gatsby).
3. TailwindCSS - One of the best atom level "Utility-First CSS" in the world, the lowest learning curve for developer AND designer (Yes, Designer may pick up faster than the developer, they got no reason to say CSS is difficult.), easier to build complex responsive layouts freely and higher productivity (such as responsive design).
4. Material-UI - One of the best CSS react components to provide simple, customizable, and accessible components (OK I just reference from its official website). The main reason for me to select it is because TailwindCSS won't provide some can component such as Button, Menu, Card and most time no one wants to spend time on that too, so I select Material-UI to handle these repeat works, but of course, we can customize it since TailwindCSS can override their layout.
5. Jest + Cypress - It is hard to say which testing tool is the best in the market since everybody is so good (such as Chai, Mocha, selenium), but jest + cypress is my favorite for unit testing + E2E testing with only reason: Nextjs suggest me to use I with them (Convention Over Configuration - in my own dev rules).

## Potential Improvement
1. Google Analytics + Mix Panel - Data collection is very important of a continuous improvement website which help company to improve UX and help to make marketing decision, so of course I would like to set up a template if I have time.
2. Sentry - To keep monitoring a production website, Sentry is one of the best tools to helping the user keep monitoring their site and helping developers to point as the error AFAP, set up a good track point need time and study (both GA and MX collection point too), so It will require much more time for that.
3. Unit Test and E2E Testing - Unit Testing and E2E Testing is a must for all product which make sure every new release has no error (at least is exist/discovered error in the past), be testing is not a must in this assessment but of course, it will be a potential improvement

## Production Consideration
1. Production Environment Setting - for all project we should split out all environment setting in the different working environment (Local, develop, testing, production), make sure every environment variables are correct before launching the production (and also make it private).
2. SEO Setting - including sitemap.xml, robots.txt, meta head, etc., if you are under the multi-domain strategy, make sure your return right things in different domain with canonical URL.
3. Static Resource - including public file e.g. SVG file, ICO file, JSON file, CSS file. check the file size and data too to make sure everything is fine.
4. CICD Pipeline - checking the production integration, unit test, and deployment pipeline cycle, to make sure everything is smooth when the production was launched. 
5. Cloud Service - check the network architecture including VPC permission, server size, auto-scaling triggers, load balancer, etc.
6. Git Version Control - double-check the developer group and permission, select a suitable Gitflow and make sure everyone follows the flow.

## Assumptions
To design the data model and data schema, I assume the data return by the API is the basic data format for each doctor and booking, to make sure all data format is the same, I just simply use the same data model as backend.
****
