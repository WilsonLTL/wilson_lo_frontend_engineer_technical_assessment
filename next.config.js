const nextTranslate = require('next-translate')

module.exports = nextTranslate({
    async rewrites() {
        return [
            {
                source: '/api/:path*',
                destination: 'https://fe-assignment-api.herokuapp.com/:path*',
                // destination: process.env.API,
            }
        ]
    }
});
