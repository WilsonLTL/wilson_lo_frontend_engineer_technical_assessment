import React from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  Button,
  Chip,
  Divider,
  IconButton,
  FormControl,
  OutlinedInput,
} from "@material-ui/core";
import { Sync, ArrowBack } from "@material-ui/icons";
import { createDoctorBooking, getAllDoctorList, getAllDoctorBooking } from "../../../apis/api";

const leftContainer = () => {
  const dispatch = useDispatch();

  const loading = useSelector((state: any) => state.loadingReducer);
  const doctorList = useSelector((state: any) => state.doctorListReducer);
  const weekDayList = ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"]

  const onChangeBookingPatientNameHandler = (event) => {
    dispatch({ type: "UPDATE_NEW_BOOKING_NAME", payload: event.target.value })
  }

  const onChangeBookingDateHandler = (event) => {
    dispatch({ type: "UPDATE_NEW_BOOKING_DATE", payload: event.target.value })
  }

  const onClickBackArrowHandler = () => {
    dispatch({ type: "UPDATE_SELECTED_DOCTOR", payload: {"selected_doctor": undefined} })
  }

  const onClickSelectBookingTimePeriod = (period) =>　{
    dispatch({ type: "UPDATE_NEW_BOOKING_START", payload: period })
    console.log(doctorList)
  }

  const onClickSubmitBookingHandler = () => {
    dispatch({ type: "UPDATE_LOADING_STATUS", payload: true })
    createDoctorBooking(doctorList.new_booking).then((res: any) => {
      dispatch({ type: "UPDATE_LOADING_STATUS", payload: false })
      dispatch({
        type: "UPDATE_SNACK_BAR",
        payload: { message: "成功預約", type: "success" },
      });
      dispatch({ type: "UPDATE_NEW_BOOKING_NAME", payload: "" })
      dispatch({ type: "UPDATE_NEW_BOOKING_DATE", payload: "" })
      dispatch({ type: "UPDATE_NEW_BOOKING_START", payload: undefined })

      refreshData()
    })
  }

  const refreshData = () => {
    dispatch({ type: "UPDATE_LOADING_STATUS", payload: true })
    getAllDoctorList().then(res => {
      dispatch({ type: "INIT_DATA", payload: { "doctor_list": res.data } })
      dispatch({ type: "UPDATE_LOADING_STATUS", payload: false })
    })
    getAllDoctorBooking().then(res => {
      dispatch({ type: "INIT_DATA", payload: { "exist_booking": res.data } })
      dispatch({
        type: "UPDATE_SNACK_BAR",
        payload: { message: "成功刷新數據", type: "success" },
      });
    })
  }

  const datePeriod = () => {
    let match_date = doctorList.selected_doctor.opening_hours.find(item => item.day == weekDayList[new Date(doctorList.new_booking.date).getDay()])
    let result_list = []
    if (match_date != undefined) {
      let start = Math.ceil(parseFloat(match_date.start))
      let end = Math.floor(parseFloat(match_date.end))
      while (start < end ) {
        result_list.push(start)
        start ++;
      }
    }
    return result_list
  }

  const checkBookinkPeriodExist = (period) => {
    let result
    let match_doctor_list = doctorList.exist_booking.filter(booking =>　booking.doctorId == doctorList.selected_doctor.id)
    match_doctor_list.find(booking => booking.start == period) != undefined ? result = true : result = false
    return result;
  }

  return (
    <div className={"border rounded shadow md:w-2/3 w-full p-4 bg-white h-full md:h-auto " + (doctorList.selected_doctor == undefined && "hidden md:flex flex-col")}>
      {
        doctorList.selected_doctor == undefined &&
        <div className="flex flex-col p-4 h-96 justify-center items-center space-y-2">
          <img className="h-32" src="https://freehunter-s3-hk-admin.s3.ap-northeast-2.amazonaws.com/img/chatroom(FL)-new-graphic-nomessage.png.png"></img>
          <span className="text-base text-blueGray-500 font-bold">
            請選擇醫生
          </span>
          <span className="text-sm text-blueGray-500 text-center font-light">
            點擊右方工作清單中的"管理預約"按鈕
          </span>
        </div>
      }
      {
        doctorList.selected_doctor != undefined &&
        <>
          <div className="flex justify-between items-center h-auto">
            <div className="flex items-center py-2">
              <IconButton onClick={onClickBackArrowHandler} className="flex md:hidden" size="small">
                <ArrowBack className="text-gray-400"/>
              </IconButton>
              <span className="text-gray-400 text-sm">{ `創建新預訂 - ${doctorList.selected_doctor.id}` }</span>
            </div>
            <IconButton
              onClick={refreshData}
              disabled={loading.status}
            >
              <Sync className="text-gray-400 w-4 h-4" />
            </IconButton>
          </div>
          <Divider />
          <div className="flex flex-row">
            <div className="md:w-1/2 w-full space-y-4 py-3 px-2">
              <div className="flex flex-row w-full justify-between space-x-2">
                <div className="flex flex-col space-y-1 w-1/2">
                  <span className="text-xs text-gray-400">醫生編號:</span>
                  <span className="text-sm">{ doctorList.selected_doctor.id }</span>
                </div>
                <div className="flex flex-col space-y-1 w-1/2">
                  <span className="text-xs text-gray-400">醫生姓名:</span>
                  <span className="text-sm">{ doctorList.selected_doctor.name }</span>
                </div>
              </div>
              <div className="flex flex-col w-full space-y-1">
                <span className="text-xs text-gray-400">醫生地址:</span>
                <span className="text-sm">{ `${doctorList.selected_doctor.address.line_2} ${doctorList.selected_doctor.address.line_1} ${doctorList.selected_doctor.address.district}` }</span>
              </div>
              <div className="flex flex-col w-full space-y-1">
                <span className="text-xs text-gray-400">患者姓名:</span>
                <FormControl className="w-full" size="small">
                  <OutlinedInput
                    className="w-full text-sm"
                    placeholder="患者姓名"
                    value={doctorList.new_booking.name}
                    onChange={onChangeBookingPatientNameHandler}
                    type="text"
                  />
                </FormControl>
              </div>
              <div className="flex flex-col w-full space-y-1">
                <span className="text-xs text-gray-400">預訂日期:</span>
                <FormControl className="w-full" size="small">
                  <OutlinedInput
                    className="w-full text-sm"
                    value={doctorList.new_booking.date}
                    onChange={onChangeBookingDateHandler}
                    type="date"
                  />
                </FormControl>
              </div>
              <div className="flex flex-col w-full space-y-1">
                <span className="text-xs text-gray-400">預訂時間:</span>
                <div className="">
                {
                  doctorList.new_booking.date != undefined &&
                  datePeriod().map(item => {
                    return (
                      <Chip 
                        disabled={checkBookinkPeriodExist(item)}
                        onClick={() => onClickSelectBookingTimePeriod(item)}
                        className={
                          "rounded m-1 hover:bg-blueGray-100 " +
                          (doctorList.new_booking.start != undefined 
                            && doctorList.new_booking.start == item && "bg-teal-400 text-white")
                        }
                        label={ `${item}:00 - ${item+1}:00` } 
                        variant={
                          (
                            doctorList.new_booking.start != undefined && 
                            doctorList.new_booking.start == item ? "default" : "outlined"
                          )
                        } />
                    )
                  })
                }
                </div>
                
              </div>
              <Button
                onClick={onClickSubmitBookingHandler}
                disabled=
                {
                  loading.status || doctorList.new_booking.date == "" || doctorList.new_booking.name == ""
                }
                className={
                  "w-full " + 
                  ( 
                    !loading.status && doctorList.new_booking.date != "" && doctorList.new_booking.name != "" ? 
                    "bg-teal-400 text-white" : "shadow"
                  )
                }
              >
                確認預訂
              </Button>
            </div>
            <Divider className="hidden sm:flex mx-2" orientation="vertical" flexItem/>
            <div className="md:w-1/2 w-full space-y-4 py-3 px-2">
              { 
                doctorList.exist_booking.filter(booking =>　booking.doctorId == doctorList.selected_doctor.id).length == 0 &&
                <div className="flex flex-col p-4 h-full justify-center items-center space-y-2">
                  <img className="h-32" src="https://freehunter-s3-hk-admin.s3.ap-northeast-2.amazonaws.com/img/chatroom(FL)-new-graphic-nomessage.png.png"></img>
                  <span className="text-base text-blueGray-500 font-bold">
                    暫時沒有預約
                  </span>
                  <span className="text-sm text-blueGray-500 text-center font-light">
                    於左方清單中填寫預約資料並建立最新的預約清單
                  </span>
                </div>
              }
              {
                doctorList.exist_booking.filter(booking =>　booking.doctorId == doctorList.selected_doctor.id).length > 0 &&
                <div>
                  <div className="flex justify-between items-center">
                    <span className="text-gray-400 text-sm">
                      預約清單  
                      { ` (${ doctorList.exist_booking.filter(booking =>　booking.doctorId == doctorList.selected_doctor.id) != undefined ?  
                        doctorList.exist_booking.filter(booking =>　booking.doctorId == doctorList.selected_doctor.id).length : 0})` }
                    </span>
                  </div>
                  <div className="flex flex-col space-y-2 my-2 h-96 overflow-y-auto">
                    {
                      doctorList.exist_booking.filter(booking =>　booking.doctorId == doctorList.selected_doctor.id).map(booking => {
                        return(
                          <div className="flex flex-col border rounded shadow-sm px-4 py-2 space-y-2 hover:bg-gray-50" key={booking.id}>
                            <div className="flex flex-row items-center space-x-2">
                              <span className="text-xs text-gray-400">患者姓名:</span>
                              <span className="text-sm">{ booking.name }</span>
                            </div>
                            <Divider></Divider>
                            <div className="flex flex-row items-center space-x-2">
                              <span className="text-xs text-gray-400">預約時間:</span>
                              <span className="text-sm">{ `${booking.start}:00` }</span>
                            </div>
                            <Button disabled={loading.status || true} 
                              className={"w-full shadow"}>
                              管理預約
                            </Button>
                          </div>
                        )
                      })
                    }
                  </div>
                </div>
              }
            </div>
          </div>
        </>
      }
    </div>
  )
}

export default leftContainer