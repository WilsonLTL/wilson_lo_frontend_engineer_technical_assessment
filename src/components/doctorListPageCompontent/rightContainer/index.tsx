import React from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  Button,
  Divider,
  IconButton,
} from "@material-ui/core";
import { Skeleton } from "@material-ui/lab";
import { Sync } from "@material-ui/icons";
import { getAllDoctorList, getAllDoctorBooking } from "../../../apis/api";

const rightContainer = () => {
  const dispatch = useDispatch();
  const loading = useSelector((state: any) => state.loadingReducer);
  const doctorList = useSelector((state: any) => state.doctorListReducer);

  const onClickSelectDoctorHandler = (doctor) => {
    dispatch({ type: "UPDATE_SELECTED_DOCTOR", payload: { "selected_doctor": doctor } })
  }

  const refreshData = () => {
    dispatch({ type: "UPDATE_LOADING_STATUS", payload: true })
    getAllDoctorList().then(res => {
      dispatch({ type: "INIT_DATA", payload: { "doctor_list": res.data } })
      dispatch({ type: "UPDATE_LOADING_STATUS", payload: false })
    })
    getAllDoctorBooking().then(res => {
      dispatch({ type: "INIT_DATA", payload: { "exist_booking": res.data } })
      dispatch({
        type: "UPDATE_SNACK_BAR",
        payload: { message: "成功刷新數據", type: "success" },
      });
    })
  }

  return (
    <div className={"border rounded shadow md:w-1/3 w-full p-4 bg-white " + (doctorList.selected_doctor != undefined && "hidden md:flex flex-col")}>
        <div className="flex justify-between items-center">
          <span className="text-gray-400 text-sm">醫生名單 { `(${ doctorList.doctor_list != undefined ?  doctorList.doctor_list.length : 0})` }</span>
          <IconButton
            onClick={refreshData}
            disabled={loading.status}
          >
            <Sync className="text-gray-400 w-4 h-4" />
          </IconButton>
        </div>
        <Divider />
        <div className="flex flex-col space-y-2 my-2 h-full md:h-96 overflow-y-auto">
            { 
              doctorList.doctor_list == undefined &&
              [...Array(3)].map(() => {
                return (
                  <div className="flex flex-col border rounded shadow-sm px-4 py-2 space-y-2 hover:bg-gray-50">
                    <div className="flex flex-row items-center space-x-2">
                      <span className="text-xs text-gray-400">醫生編號:</span>
                      <Skeleton width={60}/>
                    </div>
                    <Divider></Divider>
                    <div className="flex flex-col spacce-y-2">
                      <span className="text-xs text-gray-400">醫生姓名:</span>
                      <Skeleton />
                    </div>
                    <div className="flex flex-col spacce-y-2">
                      <span className="text-xs text-gray-400">醫生地址:</span>
                      <Skeleton />
                    </div>
                    <Button disabled={true} className="w-full shadow">
                      管理預約
                    </Button>
                  </div>
                );
              })
            }
            {
              doctorList.doctor_list != undefined &&
              doctorList.doctor_list.map((doctor) => {
                return (
                  <div className="flex flex-col border rounded shadow-sm px-4 py-2 space-y-2 hover:bg-gray-50">
                    <div className="flex flex-row items-center space-x-2">
                      <span className="text-xs text-gray-400">醫生編號:</span>
                      <span className="text-sm">{ doctor.id }</span>
                    </div>
                    <Divider></Divider>
                    <div className="flex flex-col spacce-y-2">
                      <span className="text-xs text-gray-400">醫生姓名:</span>
                      <span className="text-sm">{ doctor.name }</span>
                    </div>
                    <div className="flex flex-col spacce-y-2">
                      <span className="text-xs text-gray-400">醫生地址:</span>
                      <span className="text-sm">{ `${doctor.address.line_2} ${doctor.address.line_1} ${doctor.address.district}` }</span>
                    </div>
                    <Button disabled={loading.status} 
                      onClick={() => onClickSelectDoctorHandler(doctor)}
                      className={"w-full shadow " + (!loading.status && ("bg-teal-400 text-white"))}>
                      管理預約
                    </Button>
                  </div>
                )
              })
            }
        </div>
      </div>
  )

}

export default rightContainer;
