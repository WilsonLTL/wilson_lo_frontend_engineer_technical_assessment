import React from "react";
import { useSelector } from "react-redux";
import { Divider } from "@material-ui/core";
import { Skeleton } from "@material-ui/lab";
import {
  AccessTime,
  PeopleOutline,
  WorkOutline,
  PermContactCalendar,
} from "@material-ui/icons";

import moment from "moment";

const Banner = () => {

  const loading = useSelector((state: any) => state.loadingReducer);

  return (
    <div className="md:flex hidden md:flex-row flex-col md:space-x-4 md:space-y-0 space-y-3 w-full">
      <div className="border rounded shadow md:w-1/3 w-full bg-white">
        <div className="flex justify-between">
          <div className="flex rounded p-4 m-4 shadow bg-pink-400 justify-center items-center">
            <PeopleOutline className="w-9 h-9 text-white" />
          </div>
          <div className="flex w-full justify-center">
            <div className="flex flex-col p-4 justify-center text-right">
              <span className="text-xs text-gray-400">昨日新用戶人數</span>
              {
                loading.status ? <Skeleton /> : "-"
              }
            </div>
            <div className="flex flex-col p-4 justify-center text-right">
              <span className="text-xs text-gray-400">本日新用戶人數</span>
              {
                loading.status ? <Skeleton /> : "-"
              }
            </div>
          </div>
        </div>
        <Divider></Divider>
        <div className="flex space-x-1 items-center px-4 py-3">
          <AccessTime className="text-gray-400 w-4" />
          {
            loading.status ? <Skeleton className="w-32" /> : <span className="text-xs text-gray-400">{ moment().subtract(1, 'day').format("DD/MM/YYYY") } 至 { moment().format("DD/MM/YYYY") }</span>
          }
        </div>
      </div>
      <div className="border rounded shadow md:w-1/3 w-full bg-white">
        <div className="flex justify-between">
          <div className="flex rounded p-4 m-4 shadow bg-teal-400  justify-center items-center">
            <WorkOutline className="w-9 h-9 text-white" />
          </div>
          <div className="flex w-full justify-center">
            <div className="flex flex-col p-4 justify-center text-right">
              <span className="text-xs text-gray-400">昨日預約數目</span>
              {
                loading.status ? <Skeleton /> : "-"
              }
            </div>
            <div className="flex flex-col p-4 justify-center text-right">
              <span className="text-xs text-gray-400">本日預約數目</span>
              {
                loading.status ? <Skeleton /> : "-"
              }
            </div>
          </div>
        </div>
        <Divider></Divider>
        <div className="flex space-x-1 items-center px-4 py-3">
          <AccessTime className="text-gray-400 w-4" />
          {
            loading.status ? <Skeleton className="w-32" /> : <span className="text-xs text-gray-400">{ moment().subtract(1, 'day').format("DD/MM/YYYY") } 至 { moment().format("DD/MM/YYYY") }</span>
          }
        </div>
      </div>
      <div className="border rounded shadow md:w-1/3 w-full bg-white">
        <div className="flex justify-between">
          <div className="flex rounded p-4 m-4 shadow bg-purple-400 justify-center items-center">
            <PermContactCalendar className="w-9 h-9 text-white" />
          </div>
          <div className="flex w-full justify-center">
            <div className="flex flex-col p-4 justify-center text-right">
              <span className="text-xs text-gray-400">昨日新登記醫生</span>
              {
                loading.status ? <Skeleton /> : "-"
              }
            </div>
            <div className="flex flex-col p-4 justify-center text-right">
              <span className="text-xs text-gray-400">本日新登記醫生</span>
              {
                loading.status ? <Skeleton /> : "-"
              }
            </div>
          </div>
        </div>
        <Divider></Divider>
        <div className="flex space-x-1 items-center px-4 py-3">
          <AccessTime className="text-gray-400 w-4" />
          {
            loading.status ? <Skeleton className="w-32" /> : <span className="text-xs text-gray-400">{ moment().subtract(1, 'day').format("DD/MM/YYYY") } 至 { moment().format("DD/MM/YYYY") }</span>
          }
        </div>
      </div>
    </div>
  );
};

export default Banner;
