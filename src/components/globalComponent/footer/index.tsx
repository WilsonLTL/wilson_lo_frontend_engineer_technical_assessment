import React from "react";
import {
  Button,
  IconButton,
} from "@material-ui/core";
import {
  LinkedIn,
  Facebook,
  Instagram,
  WhatsApp,
  MailOutline,
} from "@material-ui/icons";
import Link from "next/link";

const Footer = () => {
  return (
    <footer className="py-10 px-4 bg-blueGray-200 w-full">
      <div className="flex flex-col space-y-10 sm:items-start sm:justify-start justify-center max-w-[1170px] mx-auto">
        <div className="flex flex-col sm:flex-row space-x-0 sm:space-x-10 space-y-4 sm:space-y-0 justify-center px-5">
          <div className="flex flex-col space-y-6 items-start justify-start">
            <div className="inline-flex space-x-6 items-center justify-center">
              <IconButton size="small">
                <LinkedIn className="text-blueGray-400" />
              </IconButton>
              <IconButton size="small">
                <Facebook className="text-blueGray-400" />
              </IconButton>
              <IconButton size="small">
                <Instagram className="text-blueGray-400" />
              </IconButton>
              <IconButton size="small">
                <WhatsApp className="text-blueGray-400" />
              </IconButton>
              <IconButton size="small">
                <MailOutline className="text-blueGray-400" />
              </IconButton>
              {/* <img className="w-6 h-full rounded-lg" src="https://via.placeholder.com/24x24"/> */}
            </div>
            <div className="flex w-full flex-col md:flex-row sm:flex-row space-x-4 space-y-2 md:space-y-0 items-center justify-start whitespace-nowrap">
              <div className="flex space-x-0.5 space-y-1 md:space-y-0 md:space-x-4 items-center justify-start flex-wrap">
                <Link href="/about-us">
                <div className="text-sm leading-tight text-blueGray-700 cursor-pointer hover:underline">
                </div>
                </Link>
                <div className="transform rotate-90 w-5 h-full border-green-500" />
                <Link href="/about-us#contact-us">
                <p className="text-sm leading-tight text-blueGray-700 cursor-pointer hover:underline">
                </p>
                </Link>
                <div className="transform rotate-90 w-5 h-full border-green-500" />
                <div className="transform rotate-90 w-5 h-full border-green-500" />
                <Link href="/jobs">
                <p className="text-sm leading-tight text-blueGray-700 cursor-pointer hover:underline">
                </p>
                </Link>
                <div className="transform rotate-90 w-5 h-full border-green-500" />
                <Link href="/blog">
                <p className="text-sm leading-tight text-blueGray-700 cursor-pointer hover:underline">
                </p>
                </Link>
                <div className="transform rotate-90 w-5 h-full border-green-500" />
                <Link href="/privacy-statement">
                <p className="text-sm leading-tight text-blueGray-700 cursor-pointer hover:underline">
                </p>
                </Link>
                <div className="transform rotate-90 w-5 h-full border-green-500" />
                <Link href="/terms-of-use">
                <p className="text-sm leading-tight text-blueGray-700 cursor-pointer hover:underline">
                </p>
                </Link>
                <div className="transform rotate-90 w-5 h-full border-green-500" />
                <Link href="/faq">
                <p className="text-sm leading-tight text-blueGray-700 cursor-pointer hover:underline">
                </p>
                </Link>
                <div className="transform rotate-90 w-5 h-full border-green-500" />
              </div>
            </div>
          </div>
        </div>
        <p className="text-sm leading-tight text-gray-500">
          <br />© 2021 Wilson Lo - All Rights Reserved
        </p>
      </div>
    </footer>
  )
}

export default Footer;