import React, { Component } from "react";
import { useSelector } from "react-redux";
import { Button, Snackbar } from '@material-ui/core';
import MuiAlert, { AlertProps } from '@material-ui/lab/Alert';
import { useDispatch } from "react-redux";

const SnackBar = () => {
    const snackBar = useSelector((state: any) => state.snackBarReducer);

    const dispatch = useDispatch();

    const handleClose = () => {
        dispatch({
          type: "CLOSE_SNACK_BAR",
        });
      };

    return (
        <Snackbar 
            className="w-96" open={snackBar.status} autoHideDuration={6000} 
            anchorOrigin={{vertical: 'top', horizontal: 'center'}} onClose={handleClose}>
            <MuiAlert className="w-full" elevation={6} severity={snackBar.type}>
                {snackBar.message}
            </MuiAlert>
        </Snackbar>
    )
}

export default SnackBar