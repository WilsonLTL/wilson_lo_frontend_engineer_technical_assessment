import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { getAllDoctorList, getAllDoctorBooking } from "../../../apis/api"
import Header from "../header";
import Footer from "../footer";
import SnackBar from "../snackBar"

const Layout = ({children}) => {

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch({ type: "UPDATE_LOADING_STATUS", paylaod: true })
    getAllDoctorList().then(res => {
      dispatch({ type: "INIT_DATA", payload: { "doctor_list": res.data } })
      dispatch({ type: "UPDATE_LOADING_STATUS", paylaod: false })
    })
    getAllDoctorBooking().then(res => {
      dispatch({ type: "INIT_DATA", payload: { "exist_booking": res.data } })
    })
  }, [])


  return (
    <div className="w-screen h-screen bg-blueGray-50">
      <Header />
      <div>
        {children}
      </div>
      <Footer />
      <SnackBar/ >
    </div>
  );
}

export default Layout;
