import req from "./https";


export async function getAllDoctorList () {
  return await req("get", '/doctor')
}

export async function getAllDoctorBooking () {
  return await req("get", '/booking')
}

export async function createDoctorBooking (params) {
  return await req("post", '/booking', params)
}