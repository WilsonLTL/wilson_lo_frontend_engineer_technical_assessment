import axios from "axios";

let instance = axios.create({
  baseURL: '/api',
  headers: {
    get: {
      "x-api-key": "9c62bf55-b8b6-4744-9ff0-43d29778a77f"
    },
    post: {
      "x-api-key": "9c62bf55-b8b6-4744-9ff0-43d29778a77f"
    }
  }
});

instance.interceptors.request.use(
    (config) => {
      return config;
    },
    (error) => {
      return Promise.reject(error);
    }
  );

  instance.interceptors.response.use(
    (response) => {
      return response;
    },
    (error) => {
      return Promise.reject(error);
    }
  );
  
  export default async function(method: string, url: string, data = null) {
    method = method.toLocaleLowerCase();
  
    switch (method) {
      case "get":
        return instance.get(url, { params: data });
      case "post":
        return instance.post(url, data);
      case "put":
        return instance.put(url, data);
      case "delete":
        return instance.delete(url, { params: data });
    }
  }
  