import React from "react";
import Banner from "../components/doctorListPageCompontent/banner";
import LeftContainer from "../components/doctorListPageCompontent/leftContainer";
import RightContainer from "../components/doctorListPageCompontent/rightContainer";

const DoctorList = () => {

  return (
    <div className="flex flex-col container mx-auto w-full my-5 items-center justify-center space-y-4">
      <Banner/>
      <div className="flex flex-row md:space-x-4 space-y-4 md:space-y-0 w-full">
        <LeftContainer/>
        <RightContainer/> 
      </div>
    </div>
  );
};

export default DoctorList;
