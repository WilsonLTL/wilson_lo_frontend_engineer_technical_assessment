import { UndoRounded } from '@material-ui/icons';
import { PayloadAction } from '@reduxjs/toolkit'
import { INIT_DATA, UPDATE_SELECTED_DOCTOR, UPDATE_NEW_BOOKING_NAME, UPDATE_NEW_BOOKING_DATE, UPDATE_NEW_BOOKING_START } from "../../actions/doctorListActions";
import { doctorListType } from "../../types/doctorListType"

const initState: doctorListType = {
  doctor_list: undefined,
  selected_doctor: undefined,
  new_booking: undefined,
  exist_booking: undefined
}

const doctorListReducer = (state = initState, action: PayloadAction<doctorListType>) => {
  switch (action.type) {
    case INIT_DATA:
      action.payload.doctor_list != undefined ? state.doctor_list = action.payload.doctor_list : ""
      action.payload.exist_booking != undefined ? state.exist_booking = action.payload.exist_booking : ""
      return { ...state };
    case UPDATE_SELECTED_DOCTOR:
      state.selected_doctor = action.payload.selected_doctor;
      if (action.payload.selected_doctor != undefined) {
        state.new_booking = {
          id: "4as2r43er-72d0-63331-jh4d-bbedrft5kk75",
          name: "",
          start: undefined,
          doctorId: state.selected_doctor.id,
          date: "",
          status: "confirmed"
        }
      } else {
        state.new_booking = undefined
      }
      
      return { ...state };
    case UPDATE_NEW_BOOKING_NAME:
      state.new_booking.name = action.payload
      return { ...state };
    case UPDATE_NEW_BOOKING_DATE:
      state.new_booking.date = action.payload
      return { ...state };
    case UPDATE_NEW_BOOKING_START:
      state.new_booking.start = action.payload
      return { ...state };
    default:
      return { ...state };
  }
};

export default doctorListReducer;
