export type doctorListType = {
  doctor_list: Array<doctor>,
  selected_doctor: doctor,
  new_booking: any,
  exist_booking: any
}

type doctor = {
  id: string,
  address: {
    district: string,
    line_1: string,
    line_2: string
  },
  description: string,
  name: "KWOK KWAN MAN",
  opening_hours: Array<opening_hour>
}

type opening_hour = {
  day: string,
  end: string,
  isClosed: string,
  start: string
}