export const INIT_DATA = "INIT_DATA";
export const UPDATE_SELECTED_DOCTOR = "UPDATE_SELECTED_DOCTOR";
export const UPDATE_NEW_BOOKING_NAME = "UPDATE_NEW_BOOKING_NAME";
export const UPDATE_NEW_BOOKING_DATE = "UPDATE_NEW_BOOKING_DATE";
export const UPDATE_NEW_BOOKING_START = "UPDATE_NEW_BOOKING_START";

export const initDataHandler = () => ({
   type: INIT_DATA
});

export const updateSelectedDoctorHandler = () => ({
   type: UPDATE_SELECTED_DOCTOR
});

export const updateNewBookingNameHandler = () => ({
   type: UPDATE_NEW_BOOKING_NAME
});

export const updateNewBookingDateHandler = () => ({
   type: UPDATE_NEW_BOOKING_DATE
});

export const updateNewBookingStartHandler = () => ({
   type: UPDATE_NEW_BOOKING_START
});